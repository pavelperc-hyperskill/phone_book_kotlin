package phonebook

import java.io.File
import kotlin.system.measureTimeMillis

fun linearSearch(lines: List<String>, query: String): Boolean {
    return lines.firstOrNull { it.contains(query) } != null
}

fun jumpSearch(lines: List<String>, query: String): Boolean {
    return lines.binarySearch { line -> line.substringAfter(" ").compareTo(query) } >= 0
}

fun MutableList<String>.swap(i1: Int, i2: Int) {
    val temp = this[i2]
    this[i2] = this[i1]
    this[i1] = temp
}

fun quickSort(lines: MutableList<String>) {
    // qsort
    fun partition(l: Int, r: Int): Int {
        val pivot = lines[r].substringAfter(" ")
        var i = l
        for (j in l..r) {
            if (lines[j].substringAfter(" ") < pivot) {
                lines.swap(i, j)
                i++
            }
        }
        lines.swap(i, r)
        return i
    }
    
    fun qsort(l: Int, r: Int) {
        if (l < r) {
            val p = partition(l, r)
            qsort(l, p - 1)
            qsort(p + 1, r)
        }
    }
    qsort(0, lines.size - 1)
}

fun selectionSort(lines: MutableList<String>) {
    val n = lines.size
//    val n = 30_000
//    for (i in 0 until lines.size) {
    for (i in 0 until n) {
        var minName = lines[i].substringAfter(" ")
        for (j in (i + 1) until n) {
            val name = lines[j].substringAfter(" ")
            if (name < minName) {
                lines.swap(i, j)
                minName = name
            }
        }
    }
}

fun bubbleSort(lines: MutableList<String>, timeLimit: Long) {
//    val n = 30_000
    val n = lines.size
//    val substrings = lines.map { it.substringAfter(" ") }.toMutableList()
    val startTime = System.currentTimeMillis()
    for (i in (n - 1) downTo 1) {
        if (System.currentTimeMillis() - startTime > timeLimit) {
            throw InterruptedException()
        }
        for (j in 0 until i) {
//            if (substrings[j] > substrings[j + 1]) {
            if (lines[j].substringAfter(" ") > lines[j + 1].substringAfter(" ")) {
                lines.swap(j, j + 1)
//                substrings.swap(j, j + 1)
            }
        }
    }
}

fun measureLinearSearch(lines: List<String>, queries: List<String>) : Long {
    println("Start searching (linear search)...")
    
    var entriesFound = 0
    val milliseconds = measureTimeMillis {
        queries.forEachIndexed { i, query ->
            if (linearSearch(lines, query)) {
                entriesFound++
            }
        }
    }
    println("Found $entriesFound / ${queries.size} entries. Time taken " + formatMillis(milliseconds))
    return milliseconds
}

fun formatMillis(millis: Long) = "${millis / 1000 / 60} min. ${millis / 1000 % 60} sec. ${millis % 1000} ms."

fun measureSortAndSearch(lines: MutableList<String>, queries: List<String>, timeLimit: Long) {
    println("Start sorting(bubble sort + jump search)...")
    var broken = false
    val millis1 = measureTimeMillis {
        try {
//        selectionSort(lines)
            bubbleSort(lines, timeLimit)
//        quickSort(lines)
//        lines.sortBy { it.substringAfter(" ") }
//        lines.sort()
        }catch (e: InterruptedException) {
            broken = true
        }
    }
    var entriesFound = 0
    var millis2: Long = 0
    if (broken) {
        println("STOPPED, moved to linear search")
        millis2 = measureTimeMillis {
            queries.forEachIndexed { i, query ->
                if (linearSearch(lines, query)) {
                    entriesFound++
                }
            }
        }
    } else {
        println("Done sorting. Start searching.")
        millis2 = measureTimeMillis {
            queries.forEachIndexed { i, query ->
                if (jumpSearch(lines, query)) {
                    entriesFound++
                }
            }
        }
    }
    
    println("Found $entriesFound / ${queries.size} entries. Time taken " + formatMillis(millis1 + millis2))
    println("Sorting time: " + formatMillis(millis1))
    println("Searching time: " + formatMillis(millis2))
}


fun main() {
//    println("cwd: " + System.getProperty("user.dir"))
    val directoryFile = File("C:/Users/pavel/IdeaProjects/Phone Book kotlin with git/directory.txt")
    val queriesFile = File("C:/Users/pavel/IdeaProjects/Phone Book kotlin with git/find.txt")

//    val lines = directoryFile.readLines().toMutableList()
//    val queries = queriesFile.readLines()
    val lines = directoryFile.readLines().take(30_000).toMutableList()
    val queries = lines.shuffled().take(500).map { it.substringAfter(" ") }
    
    println("directories: " + lines.size)
    println("queries: " + queries.size)
    
    val millis = measureLinearSearch(lines, queries)
//    println()
    measureSortAndSearch(lines, queries, millis)
}
