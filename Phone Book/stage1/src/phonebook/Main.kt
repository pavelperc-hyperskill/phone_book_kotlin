package phonebook

import java.io.File
import kotlin.system.measureTimeMillis

fun linearSearch(lines: List<String>, query: String): Boolean {
    return lines.firstOrNull { it.contains(query) } != null
}

fun main() {
//    println("cwd: " + System.getProperty("user.dir"))
    val directoryFile = File("C:/Users/pavel/IdeaProjects/Phone Book kotlin with git/directory.txt")
    val queries = File("C:/Users/pavel/IdeaProjects/Phone Book kotlin with git/find.txt").readLines()
    var entriesFound = 0
    
    val lines = directoryFile.readLines()
    
    println("Start searching...")
    val milliseconds = measureTimeMillis {
        queries.forEachIndexed { i, query ->
            if (linearSearch(lines, query)) {
                entriesFound++
            }
        }
    }
    
    println("Found $entriesFound / ${queries.size} entries. Time taken ${milliseconds / 1000 / 60} min. " +
            "${milliseconds / 1000 % 60} sec. ${milliseconds % 1000} ms.")
}
